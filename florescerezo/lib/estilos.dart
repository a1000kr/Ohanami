import 'package:flutter/material.dart';

const primaryColor = const Color(0xFFF461AB);
const primaryLightColor =const Color(0xFFFF94DD);
const primaryDarkColor =const Color(0xFFBE2A7C);
const secondaryColor =const Color(0xFF6BB5FF);
const secondaryLightColor =const Color(0xFFA2E7FF);
const secondaryDarkColor =const Color(0xFF2C86CB);
const primaryTextColor =const Color(0xFFFFF8F4);
const secondaryTextColor =const Color(0xFFB2FF59);
